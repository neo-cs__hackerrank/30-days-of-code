/*
 * ConditionalStatements.java
 * Created 05/may/2018 02:12:05 p. m.
 */
package mx.neocs.hr.tdc.day3;

import java.util.Scanner;

/**
 * <h1>Day 3: Intro to Conditional Statements</h1>
 * 
 * <h2>Objective</h2>
 * <p>In this challenge, we're getting started with conditional statements.</p>
 * 
 * <h2>Task</h2>
 * <p>Given an integer, n, perform the following conditional actions:</p>
 * <ul>
 *   <li>If n is odd, print Weird</li>
 *   <li>If n is even and in the inclusive range of 2 to 5, print
 *       <code>Not Weird</code></li>
 *   <li>If n is even and in the inclusive range of 6 to 20, print
 *       <code>Weird</code></li>
 *   <li>If n is even and greater than 20, print <code>Not Weird</code></li>
 * </ul>
 * 
 * <h3>Output Format</h3>
 * <p>
 *    Print <code>Weird</code> if the number is weird; otherwise, print
 *    <code>Not Weird</code>.
 * </p>
 *
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {
    
    private static final String WEIRD = "Weird";
    private static final String NOT_WEIRD = "Not Weird";

    /**
     * Read a number from the console and print <code>Weird</code> if the number
     * is weird; otherwise, print <code>Not Weird</code>.
     * 
     * @param args is not used.
     */
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt(); 
        scan.close();
        String ans;

        if (n < 1 || n > 100) {
            throw new IllegalArgumentException("The value is out of bound: 1 <= n => 100");
        }

        // if 'n' is NOT evenly divisible by 2 (i.e.: n is odd)
        if((n % 2) == 1) {
           ans = WEIRD;
        } else if (n >= 2 && n <= 5) {
           ans = NOT_WEIRD;
        } else if (n >= 6 && n <= 20) {
           ans = WEIRD;
        } else if (n > 20) {
           ans = NOT_WEIRD;
        } else {
            ans = "";
        }

        System.out.println(ans);
    }
}
