/*
 * package-info.java
 * Created 06/may/2018 03:03:14 p. m.
 */

/**
 * Package for the solution for day 1.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day1;
