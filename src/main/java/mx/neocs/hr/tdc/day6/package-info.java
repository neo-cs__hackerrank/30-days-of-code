/*
 * package-info.java
 * Created 08/may/2018 09:41:37 p. m.
 */

/**
 * Package for the solution for day 6.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day6;
