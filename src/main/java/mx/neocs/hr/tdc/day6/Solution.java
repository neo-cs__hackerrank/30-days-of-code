/*
 * Solution.java
 * Created 08/may/2018 09:41:37 p. m.
 */
package mx.neocs.hr.tdc.day6;

import java.util.Scanner;

/**
 * <h1>Objective</h1> 
 * <p>
 *    Today we're expanding our knowledge of Strings and combining it with what
 *    we've already learned about loops.
 * </p>
 * <h2>Task</h2> 
 * <p>
 *    Given a string, <em>S</em>, of length <em>N</em> that is indexed from
 *    <code>0</code> to <code>N - 1</code>, print its <em>even-indexed</em> and
 *    odd-indexed characters as <em>2</em> space-separated strings on a single
 *    line (see the Sample below for more detail).
 * </p>
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {

    /**
     * @param args not used.
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int t = Integer.parseInt(sc.nextLine());
        String[] lines = new String[t]; 
        
        for (int i = 0; i < t; i++) {
            lines[i] = sc.nextLine();
        }

        StringBuilder sb = new StringBuilder();
        
        for(String line : lines) {
            StringBuilder sbOdd = new StringBuilder();
            StringBuilder sbEven = new StringBuilder();

            for(int i = 0; i < line.length(); i++) {
                if ((i % 2) == 1) {
                    sbOdd.append(line.charAt(i));
                } else {
                    sbEven.append(line.charAt(i));                    
                }
            }

            sb.append(sbEven);
            sb.append(' ');
            sb.append(sbOdd);
            sb.append('\n');
        }

        System.out.println(sb.toString());

        sc.close();
    }

}
