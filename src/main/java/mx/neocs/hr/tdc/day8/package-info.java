/*
 * package-info.java
 * Created 10/may/2018 10:44:36 p. m.
 */

/**
 * Package for the solution for day 8.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day8;
