/*
 * Solution.java
 * Created 10/may/2018 10:45:13 p. m.
 */
package mx.neocs.hr.tdc.day8;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * <h1>Day 8: Dictionaries and Maps</h1>
 * 
 * <h2>Objective</h2> 
 * <p>
 *    Today, we're learning about Key-Value pair mappings using a Map or
 *    Dictionary data structure. Check out the Tutorial tab for learning
 *    materials and an instructional video!
 * </p>
 * 
 * <h2>Task</h2>
 * <p>
 *    Given <em>n</em> names and phone numbers, assemble a phone book that maps
 *    friends' names to their respective phone numbers. You will then be given
 *    an unknown number of names to query your phone book for. For each
 *    <em>name</em> queried, print the associated entry from your phone book on
 *    a new line in the form <code>name=phoneNumber;</code> if an entry for
 *    <em>name</em> is not found, print <code>Not found</code> instead.</p>
 * 
 * <p>
 *    <strong>Note:</strong> Your phone book should be a Dictionary/Map/HashMap
 *    data structure.
 * </p>
 * 
 * <h3>Input Format</h3>
 * 
 * <p>
 *    The first line contains an integer, <em>n</em>, denoting the number of
 *    entries in the phone book.
 * </p>
 * <p>
 *    Each of the <em>n</em> subsequent lines describes an entry in the form of
 *    <em>2</em> space-separated values on a single line. The first value is a
 *    friend's name, and the second value is an <em>8</em>-digit phone number.
 * </p>
 * 
 * <p>
 *    After the <em>n</em> lines of phone book entries, there are an unknown
 *    number of lines of queries. Each line (query) contains a <em>name</em> to
 *    look up, and you must continue reading lines until there is no more input.
 * </p>
 * 
 * <p>
 *    Note: Names consist of lower case English alphabetic letters and are first
 *    names only.
 * </p>
 * 
 * <h3>Constraints</h3>
 * <ul>
 *     <li>1 ≤ <em>N</em> ≤ 10<sup>5</sup></li>
 *     <li>1 ≤ <em>queries</em> ≤ 10<sup>5</sup></li>
 * </ul>
 * 
 * <h3>Output Format</h3>
 * 
 * <p>
 *    On a new line for each query, print <code>Not found</code> if the name
 *    has no corresponding entry in the phone book; otherwise, print the full
 *    <em>name</em> and <em>phoneNumber</em> in the format 
 *    <code>name=phoneNumber</code>.
 * </p>
 * 
 * <h3>Sample Input</h3>
 * <code>
 *   <pre>
 * 3
 * sam 99912222
 * tom 11122222
 * harry 12299933
 * sam
 * edward
 * harry
 *   </pre>
 * </code>

 * <h3>Sample Output</h3>
 * <code>
 *   <pre>
 * sam=99912222
 * Not found
 * harry=12299933
 *   </code>
 * </pre>
 *
 * <h3>Explanation</h3>
 * 
 * <p>
 *    We add the following <em>n = 3</em> (Key,Value) pairs to our map so it 
 *    looks like this:<br/>
 *    <em>phoneBook={(sam,99912222), (tom,11122222) (harry,12299933)}</em>
 * </p>
 * 
 * <p>
 *    We then process each query and print <code>key=value</code> if the queried
 *    <em>key</em> is found in the map; otherwise, we print
 *    <code>Not found</code>.
 * </p>
 *
 * Query 0: <code>sam</code>
 * Sam is one of the keys in our dictionary, so we print <code>sam=99912222</code>.
 * 
 * Query 1: <code>edward</code>
 * Edward is not one of the keys in our dictionary, so we <code>print Not found</code>.
 * 
 * Query 2: <code>harry</code>
 * Harry is one of the keys in our dictionary, so we print <code>harry=12299933</code>.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {

    /**
     * @param args
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        Map<String, Integer> addressBook = new HashMap<>();

        for(int i = 0; i < n; i++) {
            String name = in.next();
            int phone = in.nextInt();
            addressBook.put(name, phone);
        }

        StringBuilder sb = new StringBuilder();
        while(in.hasNext()){
            String s = in.next();
            sb.append(addressBook.containsKey(s) ? s + '=' + addressBook.get(s) : "Not found");
            sb.append('\n');
        }
        in.close();
        System.out.print(sb.toString());
    }

}
