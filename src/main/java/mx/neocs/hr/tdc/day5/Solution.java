/*
 * Solution.java
 * Created 07/may/2018 10:25:48 p. m.
 */
package mx.neocs.hr.tdc.day5;

import java.text.MessageFormat;
import java.util.Scanner;

/**
 * <h1>Day 5: Loops</h1>
 * <h2>Objective</h2>
 * <p>
 *    In this challenge, we're going to use loops to help us do some simple
 *    math.
 * </p>
 * <h2>Task</h2> 
 * <p>
 *    Given an integer, <em>n</em>, print its first <code>10</code> multiples.
 *    Each multiple <em>n × i</em> (where <em>1 ≤ i ≤ 10</em>) should be printed
 *    on a new line in the form: <code>n × i = result</code>.
 * </p>
 * <h3>Input Format</h3>
 * <p>A single integer, <em>n</em>.</p>
 * <h3>Constraints</h3>
 * <p><em>2 ≤ n ≤ 20</em></p>
 * <h3>Output Format</h3>
 * <p>
 *    Print <code>10</code> lines of output; each line <em>i</em> (where
 *    <em>1 ≤ i ≤ 10</em>) contains the <em>result</em> of <em>n × i</em> in the
 *    form: <code>n × i = result</code>.
 * </p>
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
public class Solution {
    
    private static final String TEMPLATE_MULTIPLY = "{0} × {1} = {2}";

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.close();

        for (int i = 1; i <= 10; i++) {
            String message = MessageFormat.format(TEMPLATE_MULTIPLY, n, i, (i * n));
            System.out.println(message);
        }
    }

}
