/*
 * package-info.java
 * Created 12/may/2018 11:47:26 p. m.
 */

/**
 * Package for the solution for day 10.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day10;
