/*
 * package-info.java
 * Created 28/sep/2018 06:52:36 p. m.
 */

/**
 * Package for the solution for day 11.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day11;
