/*
 * package-info.java
 * Created 06/may/2018 03:16:29 p. m.
 */

/**
 * Package for the solution for day 2.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day2;
