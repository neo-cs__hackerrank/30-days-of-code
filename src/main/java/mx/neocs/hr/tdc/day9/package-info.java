/*
 * package-info.java
 * Created 11/may/2018 09:17:27 p. m.
 */

/**
 * Package for the solution for day 9.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 */
package mx.neocs.hr.tdc.day9;
