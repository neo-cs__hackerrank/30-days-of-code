/*
 * PersonTest.java
 * Created 06/may/2018 01:54:01 p. m.
 */
package mx.neocs.hr.tdc.day4;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.neocs.hr.tdc.FileUtil;

/**
 * Test Day 4: Class vs. Instance
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 * @see Person
 */
public class PersonTest {
    
    private static final ByteArrayOutputStream OUT = new ByteArrayOutputStream();

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() {
        System.setOut(new PrintStream(OUT));
        System.setIn(FileUtil.getInput("mx/neocs/hr/tdc/day4/input.txt"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() {
        System.setOut(System.out);
        System.setIn(System.in);
    }

    /**
     * Test method for {@link mx.neocs.hr.tdc.day4.Person#main(java.lang.String[])}.
     */
    @Test
    public void testMain() throws IOException {
        Person.main(null);
        assertEquals(FileUtil.getOutput("mx/neocs/hr/tdc/day4/output.txt"), OUT.toString());
     }

}
