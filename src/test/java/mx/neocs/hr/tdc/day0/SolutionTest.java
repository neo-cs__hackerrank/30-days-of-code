/*
 * SolutionTest.java
 * Created 05/may/2018 02:37:13 p. m.
 */

package mx.neocs.hr.tdc.day0;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.neocs.hr.tdc.FileUtil;

/**
 * Test Day 0: Hello, World.
 * 
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 * @see Solution
 */
public class SolutionTest {

    private static final ByteArrayOutputStream OUT = new ByteArrayOutputStream(); 

    @Before
    public void setUp() {
        System.setOut(new PrintStream(OUT));
        System.setIn(FileUtil.getInput("mx/neocs/hr/tdc/day0/input.txt"));
    }

    @After
    public void tearDown() {
        System.setOut(System.out);
        System.setIn(System.in);
    }

    @Test
    public void testMain() throws IOException {
        Solution.main(null);
        assertEquals(FileUtil.getOutput("mx/neocs/hr/tdc/day0/output.txt"), OUT.toString());
    }

}
