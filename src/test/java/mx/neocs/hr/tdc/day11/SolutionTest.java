/*
 * SolutionTest.java
 * Created 23/abr/2021 10:17:29 a. m.
 */

package mx.neocs.hr.tdc.day11;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import mx.neocs.hr.tdc.FileUtil;

/**
 * @author Freddy Barrera (freddy.barrera.moo@gmail.com)
 *
 */
public class SolutionTest {

    private static final ByteArrayOutputStream OUT = new ByteArrayOutputStream();

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.setOut(new PrintStream(OUT));
        System.setIn(FileUtil.getInput("mx/neocs/hr/tdc/day11/input.txt"));
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.setOut(System.out);
        System.setIn(System.in);
    }

    /**
     * Test method for {@link mx.neocs.hr.tdc.day11.Solution#main(java.lang.String[])}.
     * @throws IOException 
     */
    @Test
    public void testMain() throws IOException {
        Solution.main(null);
        assertEquals(FileUtil.getOutput("mx/neocs/hr/tdc/day11/output.txt"), OUT.toString());
    }

}
