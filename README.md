# HackerRank: 30 Days of Code

This project contains Freddy Barrera’s solutions for HackerRank: 30 Days of
 Code, the code challenge.

## How the project has been organised?
Each challenge day has its package and also contains a Java file named
 Solution.java inside of this the solution is present. For example for day 0,
 `src/main/java/mx/neocs/hr/tdc/day0/Solution.java`. Additionally, test is
 included in `src/test/java/mx/neocs/hr/tdc/day0/SolutionTest.java`.
 
